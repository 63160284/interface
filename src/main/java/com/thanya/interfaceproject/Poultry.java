/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.interfaceproject;

/**
 *
 * @author Thanya
 */
public abstract class Poultry extends Animal implements Flyable{
    
    public Poultry(String name, int numberOfLeg) {
        super(name, numberOfLeg);
    }
    
}
