/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.interfaceproject;

/**
 *
 * @author Thanya
 */
public class Human extends LandAnimal{

    private String nickname;

    public Human(String nickname) {
        super("Human", 2);
        this.nickname = nickname;
    }

    @Override
    public void eat() {

    }

    @Override
    public void walk() {

    }

    @Override
    public void speak() {

    }

    @Override
    public void sleep() {

    }

    @Override
    public void run() {
        System.out.println("Human: run");
    }
    
}
