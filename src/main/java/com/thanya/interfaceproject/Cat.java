/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.interfaceproject;

/**
 *
 * @author Thanya
 */
public class Cat extends LandAnimal{

    private String nickname;
    public Cat(String nickname) {
        super("Cat", 4);
        this.nickname = nickname;
    }

    @Override
    public void eat() {

    }

    @Override
    public void walk() {

    }

    @Override
    public void speak() {

    }

    @Override
    public void sleep() {

    }

    @Override
    public void run() {
        System.out.println("Cat: run");
    }
    
}
