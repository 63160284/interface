/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.interfaceproject;

/**
 *
 * @author Thanya
 */
public class TestFlyable {
    public static void main(String[] args) {
        Bat bat = new Bat("BU");
        Plane plane = new Plane("Engine number I");
        bat.fly();
        plane.fly();
        Dog dog = new Dog("Som");
        Bird bird = new Bird("MUmu");
        Cat cat = new Cat("Dang");
        Human human =new Human("Tong");
        Car car = new Car("Engine number II");
        
        Flyable[] flyables = {bat,plane,bird};
        for(Flyable f : flyables){
            if(f instanceof Plane){
                Plane p = (Plane)f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }
        
        Runable[] runables = {dog, plane, cat, human, car};
        for(Runable r : runables){
            if(r instanceof Car){
                Car c = (Car)r;
                c.startEngine();
            }
            r.run();
        }
    }
}
